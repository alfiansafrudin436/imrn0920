var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]
i = 0
const read = (waktu, daftarBuku) => {
    if (daftarBuku[i] !== undefined) {
        readBooks(waktu, daftarBuku[i], (callback) => {
            i++
            read(callback, daftarBuku)
        })
    }
}

read(1000, books)