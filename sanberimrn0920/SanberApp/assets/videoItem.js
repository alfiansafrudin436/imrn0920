import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Platform, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default class videoItem extends Component {
    render() {
        let video = this.props.video;

        return (
            <View style={styles.container}>
                <Image source={{ uri: video.snippet.thumbnails.medium.url }} style={{ height: 200 }} />
                <View style={styles.descContainer}>
                    <Image source={{ uri: "https://upload.wikimedia.org/wikipedia/en/8/8b/Avatar_2_logo.jpg" }} style={{ height: 50, width: 50, borderRadius: 25 }} />
                    <View style={styles.videoDetails}>
                        <Text style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoStats}>{video.snippet.channelTitle + " , " + nFormatter(video.statistics.viewCount, 1) + ' 3 month ago'}</Text>
                    </View>
                    <TouchableOpacity>
                        <Icon name="more-vert" size={20} color='#888888'></Icon>
                    </TouchableOpacity>
                </View >
            </View >
        )
    }
}


function nFormatter(num, digits) {
    const si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "k" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    let i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
            break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + ' views';
}

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 16
    },
    videoDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    videoTitle: {
        fontSize: 16,
        color: '#3c3c3c'
    },
    videoStats: {
        fontSize: 15,
        paddingTop: 3
    }
});
