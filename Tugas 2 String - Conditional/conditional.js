// NO 1

var nama = "Jane"
var peran = "Penyihir"

if (nama == 'John' && peran == ' ') {
    console.log("nama harus diisi")
} else if (nama == 'John' && peran == '') {
    console.log("Hallo," + nama + " Pilih peranmu untuk memulai game!")
} else if (nama == 'Jenita' && peran == '') {
    console.log("Hallo," + nama + " Pilih peranmu untuk memulai game!")
} else if (nama == 'Junaedi' && peran == '') {
    console.log("Hallo," + nama + " Pilih peranmu untuk memulai game!")
} else if (nama == 'Jane' && peran == '') {
    console.log("Hallo," + nama + " Pilih peranmu untuk memulai game!")

} else if (nama == 'Jane' && peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'John' && peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Junaedi' && peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")

} else if (nama == 'Jenita' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard" + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'John' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard" + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard" + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Jane' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard" + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")

} else if (nama == 'Junaedi' && peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
} else if (nama == 'Jenita' && peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
} else if (nama == 'John' && peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
} else if (nama == 'Jane' && peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama + "")
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
}



// No 2

console.log("=================================================================================")

var hari = 1
var bulan = 10
var tahun = 2020

switch (bulan) {
    case 1: { console.log(+hari + " Januari " + tahun + ""); break; }
    case 2: { console.log(+hari + " Februari " + tahun + ""); break; }
    case 3: { console.log(+hari + " Maret " + tahun + ""); break; }
    case 4: { console.log(+hari + " April " + tahun + ""); break; }
    case 5: { console.log(+hari + " Mei " + tahun + ""); break; }
    case 6: { console.log(+hari + " Juni " + tahun + ""); break; }
    case 7: { console.log(+hari + " Juli " + tahun + ""); break; }
    case 8: { console.log(+hari + " Agustus " + tahun + ""); break; }
    case 9: { console.log(+hari + " September " + tahun + ""); break; }
    case 10: { console.log(+hari + " Oktober " + tahun + ""); break; }
    case 11: { console.log(+hari + " November " + tahun + ""); break; }
    case 12: { console.log(+hari + " Desember " + tahun + ""); break; }

}

