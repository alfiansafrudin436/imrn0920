// NO 1
const golden = () => console.log("this is goleden")
golden()
console.log("==========================")

// NO2
const myFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => { return `${firstName} ${lastName}` }
    }
}

console.log(myFunction("William", "Imoh").fullName())
console.log("==========================")

// NO 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject
console.log(firstName, lastName, destination, occupation)
console.log("==========================")

// NO 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)
console.log("==========================")

// NO 5
const planet = "earth"
const view = "glass"

const before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`

console.log(before) // Zell Liew, unaffiliated

