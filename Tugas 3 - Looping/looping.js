// NO 1

console.log("LOOPING PERTAMA")
n = 2
i = 20
while (n <= i) {
    console.log(n, "I Love Coding")
    n += 2
}
console.log("\nLOOPING KEDUA")
while (i > 0) {
    console.log(i, "I will become a mobile developer")
    i -= 2

}

// NO 2
console.log("\n")
for (var i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 !== 0) {
        console.log(i, " - I Love Coding")
    } else if (i % 2 !== 0) {
        console.log(i, " - Santai")
    } else if (i % 2 == 0) {
        console.log(i, " - Berkualitas")
    }
}

// NO 3
console.log("\n")
for (var i = 1; i <= 4; i++) {
    for (var j = 1; j <= 8; j++) {
        process.stdout.write("#")
    }
    process.stdout.write("\n")
}

// NO 3
console.log("\n")
for (var i = 1; i <= 7; i++) {
    for (var j = 1; j <= i; j++) {
        process.stdout.write("#")
    }
    process.stdout.write("\n")
}

// NO 4
console.log("\n")
for (var i = 1; i <= 8; i++) {
    for (var j = 1; j <= 8; j++) {
        if (i % 2 !== 0 && j % 2 !== 0) {
            process.stdout.write(" ")
        } else if (i % 2 == 0 && j % 2 == 0) {
            process.stdout.write(" ")
        } else {
            process.stdout.write("#")
        }

    }
    process.stdout.write("\n")
}