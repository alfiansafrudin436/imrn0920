
// NO 1
function range(startNum, finishNum) {
    var hasil = []
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            hasil.push(i)
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            hasil.push(i)
        }
    } else {
        hasil.push(-1)
    }
    return hasil
}

// NO 2
function rangeWithStep(startNum, finishNum, step) {
    var hasil = []
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum;) {
            hasil.push(i)
            i += step
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum;) {
            hasil.push(i)
            i -= step
        }
    } else {
        hasil.push(-1)
    }
    return hasil
}

// NO 3
function sum(startNum, finishNum, step) {
    var hasil = 0
    if (step == undefined) {
        step = 1
    }
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum;) {
            hasil = hasil + i
            i += step
        }
    } else {
        for (var i = startNum; i >= finishNum;) {
            hasil = hasil + i
            i -= step
        }
    }
    return hasil
}

// NO 4
function dataHandling(array = []) {
    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array[i].length; j++) {

            if (j == 0) {
                console.log("No ID :", array[i][j])
            }
            if (j == 1) {
                console.log("Nama :", array[i][j])
            }
            if (j == 2) {
                console.log("Alamat :", array[i][j])
            }
            if (j == 3) {
                console.log("Tanggal Lahir :", array[i][j])
            }
            if (j == 4) {
                console.log("Hobi :", array[i][j])
            }
        }
        process.stdout.write("\n")
    }
}

// NO 5
function balikKata(kata) {
    for (var i = kata.length - 1; i >= 0; i--) {
        process.stdout.write(kata[i])
    }
    process.stdout.write("\n\n")
}


// NO 6
function dataHandling2(array = [], start, deleteCount, add, add2, start2, deleteCount2, add3, add4) {
    console.log("ARRAY INPUT")
    console.log(array)
    console.log("=======================================================================")
    array.splice(start, deleteCount, add, add2)
    array.splice(start2, deleteCount2, add3, add4)
    console.log(array)
    var tanggal = array[3].split("/")
    var bulan = Number(tanggal[1])
    switch (bulan) {
        case 01: {
            console.log("Januari"); break;
        }
        case 02: {
            console.log("Februari"); break;
        }
        case 03: {
            console.log("Maret"); break;
        }
        case 04: {
            console.log("April"); break;
        }
        case 05: {
            console.log("Mei"); break;
        }
        case 06: {
            console.log("Juni"); break;
        }
        case 07: {
            console.log("Juli"); break;
        }
        case 08: {
            console.log("Agustus"); break;
        }
        case 09: {
            console.log("September"); break;
        }
        case 10: {
            console.log("Oktober"); break;
        }
        case 11: {
            console.log("November"); break;
        }
        case 12: {
            console.log("Desember"); break;
        }
    }

    console.log(tanggal.sort((a, b) => b - a))
    tanggal.sort((a, b) => a - b)
    var join = tanggal.join("-")
    console.log(join)
    var nama = array[1].slice(0, 15)
    console.log(nama)

}


var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log("INCREASE :", range(1, 20))
console.log("DECREASE :", range(20, 1))
console.log("INCREASE W STEP:", rangeWithStep(1, 10, 1))
console.log("DECREASE W STEP:", rangeWithStep(10, 1, 1))
console.log("INCREASE SUM:", sum(1, 10))
console.log("DECREASE SUM:", sum(10, 1))
dataHandling(input)
balikKata("aku dan kau suka DANCOW")

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input2, 1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", 4, 1, "Pria", "SMA Internasional Metro")
